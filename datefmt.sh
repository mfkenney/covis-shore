#!/bin/bash
#
# Convert date-time format:
#
#  YYYYmmddTHHMMSS --> YYYY-mm-ddTHH:MM:SS
#

d="$1"
if [[ -n "$d" ]]; then
    echo "${d:0:4}-${d:4:2}-${d:6:2}T${d:9:2}:${d:11:2}:${d:13:2}"
    exit 0
fi

exit 1
