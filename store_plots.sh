#!/bin/bash
#
# Store the COVIS isosurface plot images in a CouchDB database
#

: ${COVIS_DB="http://localhost:5984/covis_plots"}
CREDENTIALS=$(cat $HOME/.dbcredentials 2>/dev/null)

opts="-s"
[ -n "$CREDENTIALS" ] && opts="$opts --user $CREDENTIALS"

while read dataset image
do
    data_id=$(basename ${dataset%-8K.zip})
    unzip -p $dataset '*/sweep.json' | egrep -v '_id|_rev|schema' > /tmp/doc.json
    resp=$(curl $opts -X PUT "$COVIS_DB/$data_id" -H 'Content-Type: application/json' -d @/tmp/doc.json)
    if grep -q "error" <<< $resp
    then
	echo $resp
	continue
    fi
    # Get the document's revision number and upload the image as an attachment
    rev=$(echo $resp | sed -e 's/.*"rev":"\([^"]*\)".*/\1/')
    if [ -n "$rev" ]
    then
	curl $opts -X PUT "$COVIS_DB/$data_id/plot.png?rev=$rev" -H 'Content-Type: image/png' --data-binary @$image
    fi
done

    
