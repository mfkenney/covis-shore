#!/bin/bash
#
# usage: dmas_download.sh N [endtime]
#
# Download N hours of COVIS data from DMAS. If endtime is not specified, the
# current time is used. If it is specified, it can be in any format understood
# by the date(1) command.
#
# Data is written to a file named 'covis_data.zip'
#

. $HOME/bin/covislib.sh

STAGING="$COVIS_DATAROOT/staging"
LOCKFILE="/tmp/dmas_download.lock"


N=$1

if [ -z "$N" ]
then
    echo "Usage: $(basename $0) N [endtime]"
    echo
    echo "Download N hours of COVIS data. 'endtime' defaults to now"
    exit 1
fi

status=0
if (set -o noclobber;echo $$ > "$LOCKFILE") 2> /dev/null
then
    trap "rm -f $LOCKFILE; exit $?" INT TERM EXIT

    TFORMAT='%Y%m%dT%H%M%S'
    OUTPUT="$STAGING/covis_data.zip"

    if [ -n "$2" ]
    then
        t1=$($DATE --utc -d "$2" +%s)
    else
        t1=$($DATE +%s)
    fi

    # Start time in seconds since 1/1/1970
    t0=$((t1 - N*3600))

    # Convert to the format used by the web service
    t_end=$($DATE --utc -d @$t1 +${TFORMAT})
    t_start=$($DATE --utc -d @$t0 +${TFORMAT})

    add_queue rawdata

    if download "$t_start" "$t_end" "$STAGING"
    then
        for f in $(relocate "$STAGING" "$COVIS_DATAROOT/raw")
        do
            put_message rawdata "COVIS_DATAROOT=$COVIS_DATAROOT" "$f"
        done

        list_queues
    fi
    rm -f $LOCKFILE
    trap - INT TERM EXIT
else
    echo "DMAS download process already running" 1>&2
    status=1
fi

exit $status