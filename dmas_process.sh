#!/bin/bash
#
# usage: dmas_process.sh 
#

. $HOME/bin/covislib.sh

STAGING="$COVIS_DATAROOT/staging"
LOCKFILE="/tmp/dmas_process.lock"

status=0
if (set -o noclobber;echo $$ > "$LOCKFILE") 2> /dev/null
then
    trap "rm -f $LOCKFILE; exit $?" INT TERM EXIT

    add_queue rawdata

    for f in $(relocate "$STAGING" "$COVIS_DATAROOT/raw")
    do
        put_message rawdata "COVIS_DATAROOT=$COVIS_DATAROOT" "$f"
    done

    list_queues
        
    rm -f $LOCKFILE
    trap - INT TERM EXIT
else
    echo "process already running" 1>&2
    status=1
fi

exit $status