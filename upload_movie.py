#!/usr/bin/env python
#
# Upload a COVIS image movie to the Google Sites page.
#
"""
Usage: upload_movie.py controlfile
"""
import gdata.sites.client
import gdata.sites.data
import os
import sys
import json
from getpass import getpass

SITE = "covis"
PAGE = "/plume-image-movies"
CREDENTIALS = "~/.uw-google-sites"

def client_login(username, password, site=SITE, domain='uw.edu'):
    client = gdata.sites.client.SitesClient(source='apluw-covisuploader-v0', 
                                            site=site, 
                                            domain=domain)
    client.ClientLogin(username, password,
                       client.source, 
                       account_type='HOSTED')
    return client
    
def main():
    try:
        cfg = json.load(open(sys.argv[1], 'r'))
    except IndexError:
        sys.stderr.write(__doc__)
        return 1
    except Exception as e:
        sys.stderr.write('Cannot parse control file\n')
        sys.stderr.write(str(e)+'\n')
        return 1
        
    try:
        credentials = open(os.path.expanduser(CREDENTIALS), 'r').read().strip()
    except:
        username = raw_input('Enter username: ')
        password = getpass('Password: ')
    else:
        username, password = credentials.split(':')
        
    moviefile = cfg['file']
    desc = cfg['description']
    title = cfg.get('title', os.path.basename(moviefile))
    content_type = cfg.get('content_type', 'video/mp4')
    
    client = client_login(username, password) 
    uri = '%s?path=%s' % (client.MakeContentFeedUri(), PAGE)
    feed = client.GetContentFeed(uri=uri)

    attachment = client.UploadAttachment(moviefile, feed.entry[0],
                                         content_type=content_type,
                                         title=title,
                                         description=desc)
    print attachment.GetAlternateLink().href
    return 0
            
if __name__ == '__main__':
    sys.exit(main())