#!/bin/bash
#
# getdata.sh STARTDATE ENDDATE [CHUNKSIZE]
#
# Download all raw COVIS data from DMAS between start date and
# end date. CHUNKSIZE is specified in hours.
#
. $HOME/bin/covislib.sh

# Staging directory
STAGING="$COVIS_DATAROOT/staging"
# Date/time format used by DMAS
TFORMAT='%Y%m%dT%H%M%S'

dstart="$1"
dend="$2"

if [[ -z "$dstart" || -z "$dend" ]]; then
    echo "Usage: $(basename $0) STARTDATE ENDDATE [HOURS]" 1>&2
    exit 1
fi
hours="${3:-12}"

tstart=$($DATE -d "$dstart" --utc +%s)
tend=$($DATE -d "$dend" --utc +%s)
tstep=$((hours * 3600))

t=$tstart
while ((t < tend)); do
    t1=$((t + tstep))
    dt0=$($DATE -d @$t --utc +${TFORMAT})
    dt1=$($DATE -d @$t1 --utc +${TFORMAT})
    echo "Downloading dataset $dt0 -> $dt1"
    if download "$dt0" "$dt1" "$STAGING"; then
        relocate "$STAGING" "$COVIS_DATAROOT/raw"
    else
        echo "$dt0,$dt1" 1>&2
    fi
    t=$t1
done

exit 0
