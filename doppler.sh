#!/bin/bash
#
# Process a directory of DOPPLER data-sets
#
. $HOME/bin/covislib.sh

if [ $# -lt 1 ]
then
    echo "Usage: $(basename $0) datadir"
    exit 1
fi

add_queue tasks

rawdir="$1"
for f in $(find $rawdir -name '*DOPPLER*' -print)
do
    echo -n "$(basename $f) ... "
    result="$(process_doppler $f)"
    if [ -n "$result" ]
    then
        put_message tasks "COVIS_DATAROOT=$COVIS_DATAROOT" $result
        echo "added to processing queue."
    else
        echo "unpacking failed!"
    fi
done