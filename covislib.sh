#!/bin/bash
#
# Variables and functions for COVIS data downloading and processing
#

# Make sure Matlab is in our path
PATH=$PATH:/usr/local/bin
export PATH
# This is needed by Matlab
MLM_LICENSE_FILE=27000@lmas.engr.washington.edu,27000@nexus.engr.washington.edu,27000@persephone.engr.washington.edu
export MLM_LICENSE_FILE

# GNU date is required, installed as 'gdate' on OSX/FreeBSD
if which gdate 1> /dev/null 2>&1
then
    DATE=gdate
else
    DATE=date
fi

# DMASUSER can be overridden by an environment variable
: ${DMASUSER=12071}
: ${COVIS_DATAROOT=$HOME/data}
: ${COVIS_DISPLAY=':2'}
: ${COVIS_LOGDIR=/var/tmp}

# These should not be changed
DMASDEV=19003
DMASFILE=32
URL="http://dmas.uvic.ca/FiledownloadService"

download ()
{
    t_start="$1"
    t_end="$2"
    inbox="$3"
    output="$inbox/covis_data.zip"

    echo "$URL?id=${DMASUSER}&datefrom=${t_start}&dateto=${t_end}&deviceid=${DMASDEV}&datafiletypeid=${DMASFILE}"
    if curl -s "$URL?id=${DMASUSER}&datefrom=${t_start}&dateto=${t_end}&deviceid=${DMASDEV}&datafiletypeid=${DMASFILE}" > $output
    then
        :
    else
        echo "Data download failed!"
        return 1
    fi

    cd "$inbox"
    if unzip "$output" 2> /dev/null
    then
        rm -f "$output"
    else
        echo "Unzip failed!"
        return 1
    fi
}

# Determine the directory to store an archive. The directory tree is based on
# the date/time that the data was collected. The information is encoded in the
# filename.
getdir ()
{
    arfile="$(basename $1)"
    echo "$($DATE -u -d "${arfile:21:8} ${arfile:30:4}" +'%Y/%m/%d')"
}

relocate ()
{
    inbox="$1"
    destdir="$2"

    cd "$inbox"
    for f in $(find . -name '*.tar.gz' -print)
    do
        if [ -s "$f" ]
        then
            datadir=$(dirname "$f")
            dirs=$(getdir "$f")
            mkdir -p "$destdir/$dirs"
            dest="$destdir/$dirs/$(basename $f)"

            if cmp -s "$f" "$dest"
            then
                # This file has already been processed
                rm -f "$f"
            else
                # Move the file from the staging directory to the final location
                mv "$f" "$dest"
                echo "$dest"
            fi

            # If the directory is empty, delete it
            nfiles=$(ls -1 $datadir | wc -l)
            if [ $nfiles -eq 0 ]
            then
                rmdir $datadir 1> /dev/null 2>&1
            fi
        fi
    done

}

# Unpack the dataset and create a Matlab script to process it
unpack_data ()
{
    infile="$1"
    dirs="$(getdir $(basename $infile))"
    name=$(basename $infile .tar.gz)
    mode=$(echo $name | cut -f2 -d-)

    decdir="$COVIS_DATAROOT/decimated/$dirs"
    rawdir="$COVIS_DATAROOT/raw/$dirs"
    procdir=
    imgdir=
    imgprocdir=
    subsample=4

    case "$mode" in
    IMAGING)
        imgprocdir="$COVIS_DATAROOT/processed/$dirs"
        imgdir="$COVIS_DATAROOT/images/$dirs"
        procdir=
        subsample=4
        ;;
    DOPPLER*)
        subsample=8
        procdir="$COVIS_DATAROOT/processed/$dirs"
        imgdir=
        imgprocdir=
        ;;
    BATHY*)
        subsample=8
        procdir=
        imgdir=
        imgprocdir=
        ;;
    DIFFUSE*)
        subsample=1
        procdir=
        imgdir=
        imgprocdir=
        ;;
    esac

    # If the decimated zip file already exists and is not empty, skip the processing step
    zipname="${name}-$((34/subsample))K.zip"
    if [ -s "$decdir/$zipname" ]
    then
        echo "Decimated data already exists: $decdir/$zipname" 1>&2
        return 0
    fi


    mkdir -p $decdir $procdir $imgdir
    # Extract the tar archive to $decdir. This is much faster than having the
    # Matlab code do it.
    mkdir "$decdir/$name"
    if tar -C "$decdir/$name" -x -z --strip=1 -f "$infile"
    then
        # The new doppler processing code uses the raw data rather than
        # the decimated data. It expects to find its input (tarfile or
        # directory) under $rawdir. Provide a link so the Matlab code
        # doesn't need to unpack the data.
        [ "$mode" = "DOPPLER" ] && ln -s "$decdir/$name" "$rawdir/$name"
    else
        [ -d "$decdir/$name" ] && rm -rf "$decdir/$name"
        return 1
    fi
    pushd $COVIS_DATAROOT/matlab 1> /dev/null 2>&1

    # Edit the template to create a custom version of covis_test
    scriptname="covis_test_$(uuid -F SIV)"
    sed -e "s!@TARFILE@!$infile!g" \
        -e "s!@DECDIR@!$decdir!g" \
        -e "s!@PROCDIR@!$procdir!g" \
        -e "s!@IMGPROCDIR@!$imgprocdir!g" \
        -e "s!@IMGDIR@!$imgdir!g" \
        -e "s!@SUBSAMPLE@!$subsample!g" covis_test.m.in > ${scriptname}.m
    popd 1> /dev/null 2>&1
    echo "$name $mode $scriptname $decdir $rawdir"
}

# Process a DOPPLER data-set
process_doppler ()
{
    # rawpath <- $COVIS_DATAROOT/raw/YYYY/MM/DD/DATASETNAME.tar.gz
    rawpath="$1"
    name=$(basename $rawpath .tar.gz)
    mode=$(echo $name | cut -f2 -d-)
    dirs=$(dirname $rawpath | sed -e 's!.*/raw/\(.*\)$!\1!')

    [ "$mode" = "DOPPLER" ] || return

    procdir="$COVIS_DATAROOT/processed/$dirs"
    decdir="$COVIS_DATAROOT/decimated/$dirs"
    rawdir="$(dirname $rawpath)"
    imgdir=
    imgprocdir=
    subsample=8

    mkdir -p $decdir $procdir
    # Extract the tar archive to $decdir. This is much faster than having the
    # Matlab code do it.
    mkdir "$decdir/$name"
    if tar -C "$decdir/$name" -x -z --strip=1 -f "$rawpath"
    then
        # The new doppler processing code uses the raw data rather than
        # the decimated data. It expects to find its input (tarfile or
        # directory) under $rawdir. Provide a link so the Matlab code
        # doesn't need to unpack the data.
        ln -s "$decdir/$name" "$rawdir/$name"
    else
        [ -d "$decdir/$name" ] && rm -rf "$decdir/$name"
        return 1
    fi
    pushd $COVIS_DATAROOT/matlab 1> /dev/null 2>&1

    # Edit the template to create a custom version of covis_test
    scriptname="covis_test_$(uuid -F SIV)"
    sed -e "s!@TARFILE@!$rawpath!g" \
        -e "s!@DECDIR@!$decdir!g" \
        -e "s!@PROCDIR@!$procdir!g" \
        -e "s!@IMGPROCDIR@!$imgprocdir!g" \
        -e "s!@IMGDIR@!$imgdir!g" \
        -e "s!@SUBSAMPLE@!$subsample!g" covis_test.m.in > ${scriptname}.m
    popd 1> /dev/null 2>&1
    echo "$name $mode $scriptname $decdir $rawdir"
}

# Run the Matlab script to decimate the data and produce a quick-look image
run_matlab ()
{
    name="$1"
    mode="$2"
    scriptname="$3"
    workdir="$4"
    rawdir="$5"

    pushd $COVIS_DATAROOT/matlab 1> /dev/null 2>&1

    # Cron does not set the USER env variable which the VNC server requires
    if [ -z "$USER" ]
    then
        export USER=$LOGNAME
    fi

    # Matlab uncompresses .tar.gz archives into TMP before extracting the files. The
    # doppler runs require 12+ GBytes of free space so TMP needs to be on a filesystem
    # with lots of space
    export TMP=/opt/tmp

    # Start a VNC server so we can use the OpenGL renderer in Matlab. Note that the VNC
    # server requires you to set an access-control password or it will not start. You
    # can set this by running the vncpasswd program (as the user that will run this script).
    vncserver $COVIS_DISPLAY -geometry 1024x768 1> "$COVIS_LOGDIR/vnc-${COVIS_DISPLAY}.out" 2>&1

    # Run the Matlab processing software. Evaluating the output of this pipeline
    # will set a couple of shell variables containing the name of the decimated
    # archive file and the plot image file
    eval "$(matlab -display $COVIS_DISPLAY -nodesktop -nosplash -r $scriptname 2>&1 |\
           tee $COVIS_LOGDIR/covis_matlab-${mode}-${COVIS_DISPLAY}.out |\
           sed -n -e 's!^@@\(.*\)!\1!p')"

    rm -f ${scriptname}.m

    if [ -n "$imgfile" ]
    then
        echo "$zipfile $imgfile"
        imgfile=
    fi

    # Remove the MAT file used to create the plot
    if [ -n "$matfile" ]
    then
        rm -f "$matfile"
        matfile=
    fi

    # Kill the VNC server
    vncserver -kill $COVIS_DISPLAY 1>> $COVIS_LOGDIR/vnc-${COVIS_DISPLAY}.out 2>&1

    # Remove the scratch directories that the Matlab process leaves under $workdir
    pushd $workdir 1> /dev/null 2>&1
    for f in ${name}*
    do
        [ -d "$f" ] && rm -rf "$f"
    done
    popd 1> /dev/null 2>&1

    [ -L "$rawdir/$name" ] && rm -f "$rawdir/$name"

    popd 1> /dev/null 2>&1
}

# Use Matlab to decimate the data and produce a quick-look image
process ()
{
    echo -n "Unpacking $1 ... " 1>&2
    t0=$($DATE +%s)
    result="$(unpack_data $1)"
    status=$?
    t1=$($DATE +%s)
    echo "done ($status). ($((t1 - t0)) secs)" 1>&2

    echo -n "Processing $1 ... " 1>&2
    t0=$($DATE +%s)
    run_matlab $result >> $COVIS_LOGDIR/filelist
    status=$?
    t1=$($DATE +%s)
    echo "done ($status). ($((t1 - t0)) secs)" 1>&2
}

####################################################################
#
# Primitives to manage message-queues using Redis lists
#
####################################################################

qget_nowait ()
{
    redis-cli lpop "$1"
}

qget ()
{
    timeout=${2:-0}
    msg="$(redis-cli blpop "$1" $timeout)"
    sed -n -e 2p <<< "$msg"
}

qpeek ()
{
    redis-cli lindex "$1" 0
}

qput ()
{
    q="$1"
    shift
    redis-cli rpush "$q" "$*"
}

qlen ()
{
    redis-cli llen "$1"
}

qdel ()
{
    redis-cli del "$1"
}

# Add a new queue
add_queue ()
{
    redis-cli sadd "covis:queues" "$1" 1> /dev/null
}

list_queues ()
{
    redis-cli smembers "covis:queues" |\
    while read qname
    do
        printf "%16s : %d messages\n" $qname $(qlen $qname)
    done
}

qcheck ()
{
    redis-cli ping 1> /dev/null 2>&1
    return $?
}

####################################################################
#
# Higher-level message queue functions.
#
####################################################################

queue_check ()
{
    if qcheck
    then
        :
    else
        echo "Message queue not available, aborting ..."
        sleep 5
    fi
}

# Get the next message from a queue
get_message ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        echo $(qget_nowait $qname)
    fi
}

# Block until a message arrives on the queue
wait_for_message ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        echo $(qget $qname "$2")
    fi
}

# Append a message to a queue
put_message ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        shift
        echo $(qput $qname "$*")
    fi
}

# Publish the contents of a file to a message queue
queue_file ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        fname="$2"
        echo $(qput $qname "$(openssl base64 -e < $fname | tr -d '\n')")
    fi
}

unqueue_file ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        fname="$2"

        contents="$(get_message $qname)"
        if [ -n "$contents" ]
        then
            echo "$contents" | dd cbs=76 conv=unblock 2> /dev/null |\
              openssl base64 -d -out "$fname"
        fi
    fi
}

rm_queue ()
{
    if [ -n "$1" ]
    then
        redis-cli srem "covis:queues" "$1" 1> /dev/null
        echo $(qdel "$1")
    fi
}

# Peek at the next message from a queue
peek_message ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        echo $(qpeek $qname)
    fi
}

# Return the length of a queue
queue_len ()
{
    if [ -n "$1" ]
    then
        qname="$1"
        echo $(qlen $qname)
    fi
}
