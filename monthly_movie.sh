#!/bin/bash
#
# Create a monthly COVIS Image movie. This script should be run from a cron job on
# the first day of each month.
PATH=$HOME/bin:/bin:/usr/bin:/usr/local/bin:/opt/local/bin
export PATH

# GNU date is required, installed as 'gdate' on OSX/FreeBSD
if which gdate 1> /dev/null 2>&1
then
    DATE=gdate
else
    DATE=date
fi

: ${OUTBOX=/var/tmp/videos}
mkdir -p $OUTBOX

tref=$($DATE -d '1 month ago' +%s)
days=$($DATE -d yesterday +%d)

outfile=$($DATE -d @$tref +'covis_%Y%m.mp4')
dstart=$($DATE -d @$tref +'%Y/%m/%d')
pathname="$HOME/Videos/$outfile"

rm -f "$pathname"
covis_movie.py -f -o $pathname $dstart $days
cat<<EOF> $OUTBOX/covis_movie.json
{
    "file": "$pathname",
    "description": "$($DATE -d @$tref +'%B %Y')"
}
EOF
