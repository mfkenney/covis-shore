#!/bin/bash

. $HOME/bin/covislib.sh

add_queue rawdata

for file
do
    [ -f "$file" ] && put_message rawdata "$file"
done
