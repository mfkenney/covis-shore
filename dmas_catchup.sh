#!/bin/bash
#
# This script operates in two modes, adding missing COVIS dataset information to
# an AMQP message queue and processing the oldest message from that queue.
#
# The input file, if present, is in CSV format with the following fields:
#
#   YEAR, MONTH_NAME, DAY, HOUR
#
# Output is sent to an AMQP message queue named 'missing'. Each message has the following
# space separated fields:
#
#   DATA_ROOT END_TIME N_HOURS
#
# DATA_ROOT := top level data directory
# END_TIME  := data-set end time in seconds since 1/1/1970 UTC
# N_HOURS   := number of hours of data to download
#
#
export PATH=$PATH:$HOME/bin

infile="${1:-/dev/null}"
exec <$infile

. $HOME/bin/covislib.sh

# Time we changed to ~/data2 (2012-04-25 00:00:00 UTC)
DATA2_START=1335312000
N=1

add_queue missing

while IFS=, read year mname day hour
do
    echo "Adding entry: $year $mname $day $hour"
    if [ "$hour" = "whole day" ]
    then
        hours="0 3 6 9 12 15 18 21"
    else
        hours="$hour"
    fi

    for h in $hours
    do    
        ref="$day $mname $year ${h}:00:00"
        t_start=$($DATE --utc -d "$ref" +%s)
        t_end=$((t_start + N*3600))
        if ((t_start < DATA2_START))
        then
            export COVIS_DATAROOT=$HOME/data1
        else
            export COVIS_DATAROOT=$HOME/data
        fi
        put_message missing $COVIS_DATAROOT $t_end $N
    done
done

ntasks=$(queue_len tasks)
nfiles=$(queue_len rawdata)
count=2

while ((count > 0))
do
    if ((ntasks <= 2 && nfiles <= 4))
    then
        msg="$(get_message missing)"
        if [ -n "$msg" ]
        then
            echo "Downloading missing dataset"
            set -- $msg
            export COVIS_DATAROOT=$1
            t_end=$2
            N=$3
            endtime="$($DATE --utc -d @$t_end +'%Y-%m-%d %H:%M:%S')"
            dmas_download.sh $N "$endtime"
        fi
    else
        echo "Too many pending jobs, processing deferred."
    fi
    ((count--))
done

