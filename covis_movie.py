#!/usr/bin/env python
#
#
"""
%prog [options] STARTDATE N

Create an MP4 video file of N days worth of COVIS image plots
starting on STARTDATE (YYYY/MM/DD). Requires ffmpeg to do the
actual video encoding.
"""
import couchdb
import subprocess
import Image
import logging
import os
from datetime import datetime, timedelta
from tempfile import mkdtemp
from shutil import rmtree
from cStringIO import StringIO
from optparse import OptionParser
from contextlib import contextmanager

@contextmanager
def pushd(dirname):
    """
    Context manager to temporarily work in a new directory
    """
    cwd = os.getcwd()
    os.chdir(dirname)
    try:
        yield
    finally:
        os.chdir(cwd)

def make_video(workdir, pattern, outfile, opts=[]):
    logging.info('Creating video')
    cmd = ['ffmpeg'] + opts + ['-y', '-i', pattern] + [outfile]
    logging.debug(str(cmd))
    return subprocess.call(cmd, cwd=workdir)

def get_frames(db, start, end, workdir, small=True):
    """
    Download plot images from the CouchDB server, resize if needed, and
    store in the working directory. Returns a tuple containing the
    filename pattern for the image files and the file count.
    """
    pattern = 'f%06d.png'
    logging.info('Start date: %s', str(start))
    logging.info('End date: %s', str(end))
    frame = 1
    with pushd(workdir):
        for result in db.view('plotviewer/plots', startkey=(start.timetuple())[0:3], 
                              endkey=(end.timetuple())[0:3]):
            plot = db.get_attachment(result.id, 'plot.png')
            if plot:
                outfile = pattern % (frame,)
                logging.debug('Downloading frame %d (%s)', frame, outfile)
                frame = frame + 1
                if small:
                    im = Image.open(StringIO(plot.read()))
                    im.resize((600, 450), Image.ANTIALIAS).save(outfile)
                else:
                    outf = open(outfile, 'wb')
                    outf.write(plot.read())
                    outf.close()
    return pattern, frame

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='http://localhost', dbname='covis_plots',
                        outfile='covis_movie.mp4', halfsize=True,
                        verbose=False, keep=False, workdir=None,
                        rate=6)
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='URL',
                      help='specify CouchDB server url (default: %default)')
    parser.add_option('-d', '--dbname',
                      type='string',
                      dest='dbname',
                      metavar='NAME',
                      help='specify database name (default: %default)')
    parser.add_option('-o', '--outfile',
                      type='string',
                      dest='outfile',
                      metavar='NAME',
                      help='specify output filename (default: %default)')
    parser.add_option('-f', '--fullsize',
                      action='store_false',
                      dest='halfsize',
                      help='use full-size images for video')
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='more diagnostic output')
    parser.add_option('-k', '--keep',
                      action='store_true',
                      dest='keep',
                      help='keep temporary files')
    parser.add_option('-w', '--workdir',
                      type='string',
                      dest='workdir',
                      metavar='NAME',
                      help='specify working directory for movie frames (implies --keep)')
    parser.add_option('-r', '--rate',
                      type='int',
                      dest='rate',
                      help='specify movie frame rate in frames/sec (default: %default)')

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    start = datetime.strptime(args[0], '%Y/%m/%d')
    days = int(args[1])

    logging.basicConfig(level=(opts.verbose and logging.DEBUG or logging.INFO),
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')

    svr = couchdb.client.Server(opts.server)
    db = svr[opts.dbname]

    if opts.workdir:
        workdir = opts.workdir
        opts.keep = True
    else:
        workdir = mkdtemp()
        
    end = start + timedelta(days=days)
    now = datetime.utcnow()
    copyright = 'Copyright %d UW Applied Physics Lab and Rutgers University' % now.year
    metadata = {
                'title': 'COVIS Plume Image Movie',
                'date': end.strftime('%Y-%m-%dT00:00:00Z'),
                'description': 'https://sites.google.com/a/uw.edu/covis/',
                'copyright': copyright
               }
    md_opts = []
    for k, v in metadata.items():
        md_opts.append('-metadata')
        md_opts.append('%s="%s"' % (k, v))
    
    logging.debug('Working directory %s', workdir)
    pattern, frame_count = get_frames(db, start, end, workdir, opts.halfsize)
    logging.info('Downloaded %d video frames', frame_count)
    retval = make_video(workdir, pattern, os.path.abspath(opts.outfile), 
                        ['-r', str(opts.rate), 
                         '-b', '1800',
                         '-timestamp', 'now'] + md_opts)
    if not opts.keep:
        logging.info('Removing temporary files')
        rmtree(workdir)

    return retval

if __name__ == '__main__':
    import sys
    sys.exit(main())
