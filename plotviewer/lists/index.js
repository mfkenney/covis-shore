function(head, req) {
    var ddoc = this;
    var Mustache = require("lib/mustache");
    var List = require("vendor/couchapp/lib/list");
    var path = require("vendor/couchapp/lib/path").init(req);
    var Atom = require("lib/atom");
    var util = require('lib/utils');

    var stylePath = path.asset('style');
    var indexPath = path.list('index','recent-items',{descending:true, limit:1});
    var feedPath = path.list('index','recent-items',{descending:true, limit:40, format:"atom"});

    provides("html", function() {
		 var row = getRow();
		 var plot = ['', req.path[0], row.id, 'plot.png'].join('/');

		 var data = {
		     header: {
			 feedPath: feedPath,
			 index: indexPath
			 },
		     feedPath: feedPath,
		     stylePath: stylePath,
		     name: row.id,
		     plot_url: plot,
		     older: function() {
			 return path.older(row.key);
		     }
		 };

		 return Mustache.to_html(ddoc.templates.index, data, 
					 ddoc.templates.partials, send);
	     });

    provides('atom', function() {
		 var row = getRow();
		 var updated = (row ? util.parseISO8601(row.key) : new Date());

		 var feedHeader = Atom.header({
						  updated: updated,
						  title: 'COVIS Image Plots',
						  feed_id: path.absolute(indexPath),
						  feed_link: path.absolute(feedPath)
					      });
		 send(feedHeader);

		 if(row) {

		     do {
			 var doc = row.value;
			 var prefix = path.absolute('/'+req.info.db_name+'/'+row.id);
			 var enclosures = [];
			 for(var name in doc._attachments) {
			     var obj = doc._attachments[name];
			     enclosures.push({
						 href: prefix+'/'+name,
						 type: obj.content_type,
						 length: obj.length
					     });
			 }

			 var feedEntry = Atom.entry({
							entry_id: prefix,
							title: 'COVIS Image',
							updated: util.parseISO8601(row.key),
							content_type: 'text',
							content: 'COVIS sonar image taken at ' + row.key,
							author: 'covis@neptuneca',
							enclosures: enclosures
						    });
			 send(feedEntry);
		     } while(row = getRow());
		 }
		 return '</feed>';
	     });
}
