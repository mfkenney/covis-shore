function(doc) {
    //!code helpers/utils.js
    if (doc.timestamp) {
	var t = new Date(doc.timestamp[0]*1000 + doc.timestamp[1]/1000.);
	emit(isoformat(t), doc);
    }
};
