function(doc) {
    if (doc._attachments && doc._attachments['plot.png']) {
	var t = new Date(doc.timestamp[0]*1000 + doc.timestamp[1]/1000.);
	emit([t.getUTCFullYear(),
	      t.getUTCMonth()+1,
	      t.getUTCDate(),
	      t.getUTCHours(),
	      t.getUTCMinutes(),
	      t.getUTCSeconds()
	     ], null);
    }
};
