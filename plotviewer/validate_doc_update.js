function (newDoc, oldDoc, userCtx) {
 
    function forbidden(message) {    
	throw({forbidden : message});
    }
    
    function unauthorized(message) {
	throw({unauthorized : message});
    }

    function require(beTrue, message) {
	if (!beTrue) forbidden(message);
    }

    if(newDoc.mode) {
	require(userCtx.roles.indexOf("data_source") != -1, "You are not permitted to add/change data");
	require(newDoc.timestamp, "Timestamp must be specified");
    }
}
